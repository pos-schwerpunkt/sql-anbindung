USE regtool;
/* DROP DATABASE regtool; */

/* ADD CLASS */
INSERT IGNORE INTO tbl_jahrgang (jName, jJahr) VALUES ('4AKIF', 2020);

/* ADD PUPIL */
INSERT IGNORE INTO tbl_schueler (sName, sGeb, sMatrikelNr) VALUES ('Alice', STR_TO_DATE('17.04.1980',GET_FORMAT(DATE,'EUR')), 123465);

/* ADD TEACHER */
INSERT IGNORE INTO tbl_lehrer (lName, lGeb) VALUES ('Müller', STR_TO_DATE('17.04.1980',GET_FORMAT(DATE,'EUR')));

/*  ADD SUBJECT */
INSERT IGNORE INTO tbl_faecher (fName, fHours) VALUES('Chemie', 4);

/* ADD PUPIL TO CLASS */
UPDATE tbl_schueler set jID = '1' WHERE sID = '1';

/* ADD Klassenvorstand TO PUPIL */
UPDATE tbl_schueler set lID = '1' WHERE sID = '1';

/* ADD PUPIL TO TEACHER AND VICE VERSA */
INSERT IGNORE INTO tbl_schueler_lehrer (lID, sID) VALUES (1, 1);

/* SHOW ALL PUPIL OF A TEACHER OR ALL TEACHER OF A PUPIL */
SELECT sl.slID AS ID, sName AS Schülername, s.sID as ID, s.sMatrikelnr AS Nummer, t.lName AS Lehrperson, t.lID AS ID FROM tbl_schueler_lehrer sl 
INNER JOIN tbl_schueler s ON sl.sID = s.sID
INNER JOIN tbl_lehrer t ON sl.lID = t.lID
WHERE s.sID = 1; /* ID from student */
/* WHERE t.lID = 1; */ /* ID from teacher */

/* ADD SUBJECT TO TEACHER */
INSERT IGNORE INTO tbl_faecher_specs (fID, lID) VALUES (1, 1);

/* ADD SUBJECT TO PUPIL */
INSERT IGNORE INTO tbl_faecher_specs (fID, sID) VALUES (1, 1);

/* ADD GRADES TO PUPIL */
INSERT IGNORE INTO tbl_beurteilung (sID, lID, fID, bNote, bDatum) VALUES (1, 1, 1, 3, STR_TO_DATE('14.06.2020',GET_FORMAT(DATE,'EUR')));

/* SHOW ALL	PUPIL OF A CLASS */
SELECT * FROM tbl_schueler WHERE jID = 1;

/* SHOW ALL TEACHERS OF A SUBJECT */
SELECT fID AS ID, lID AS Teacher FROM tbl_faecher_specs WHERE fID =  1;

/* SHOW ALL PUPIL OF A SUBJECT */
SELECT fID AS ID, sID AS Pupil FROM tbl_faecher_specs WHERE fID = 1;

/* SHOW ALL SUBJECTS OF A TEACHER */
SELECT fID AS ID, lID AS Teacher FROM tbl_faecher_specs WHERE lID = 1;

/* SHOW ALL SUBJECTS OF A PUPIL */
SELECT fID AS ID, sID AS Pupil FROM tbl_faecher_specs WHERE sID = 1;

/* SHOW GRADES OF A PUPIL */
SELECT * FROM tbl_beurteilung WHERE sID = 1;

/* DELETE ALL DATA IN TABLES */
/*
SET FOREIGN_KEY_CHECKS = 0; 
TRUNCATE tbl_beurteilung;
TRUNCATE tbl_schueler;
TRUNCATE tbl_lehrer;
TRUNCATE tbl_jahrgang;
TRUNCATE tbl_faecher;
SET FOREIGN_KEY_CHECKS = 1; 
*/
SELECT * FROM tbl_jahrgang;
SELECT * FROM tbl_schueler;
SELECT * FROM tbl_lehrer;
SELECT * FROM tbl_faecher;
SELECT * FROM tbl_beurteilung;