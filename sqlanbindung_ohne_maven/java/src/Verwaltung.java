import java.util.Scanner;

public class Verwaltung {
    private static Scanner scan = new Scanner(System.in);
    private static MySQLConnect con = new MySQLConnect();
    private static Lehrer teacher = new Lehrer();
    private static Student student = new Student();
    private static Faecher subject = new Faecher();
    private static Jahrgang jahrgang = new Jahrgang();

    public static void main(String[] args) {
        menu();
    }

    public static void advancedMenu() {
        System.out.println("-------------------------------------");
        System.out.println("[5] Add pupil to class");
        System.out.println("[6] Add pupil to teacher");
        System.out.println("[7] Add subject to teacher");
        System.out.println("[8] Add grades to pupil");
        System.out.println("[9] Show all pupil of a class");
        System.out.println("[10] Show all pupil of a teacher");
        System.out.println("[11] Show all subjects of a teacher");
        System.out.println("[12] Show all subjects of a pupil");
        System.out.println("[13] Show all teacher of a subject");
        System.out.println("[14] Show all pupil of a subject");
        System.out.println("[15] Show grades of a pupil");
    }

    public static void menu() {
        String choice;
        String name, year, number, sql;
        do {
            System.out.println("######################################");
            System.out.println("   ---   School Registry Tool   ---   ");
            System.out.println("######################################");
            System.out.println("[1] Add class");
            System.out.println("[2] Add teacher");
            System.out.println("[3] Add pupil");
            System.out.println("[4] Add subject");
            if (
                    con.NumberOfRows("tbl_jahrgang") != 0
                            && con.NumberOfRows("tbl_lehrer") != 0
                            && con.NumberOfRows("tbl_schueler") != 0
                            && con.NumberOfRows("tbl_faecher") != 0
            ) {
                advancedMenu();
            }
            System.out.println("[0] Quit tool (WARNING: deletes all data)");
            System.out.println("######################################");
            System.out.print("Your choice: ");
            do {
                choice = scan.nextLine();
                if (!choice.matches("[0-9]{1,2}")) {
                    System.out.print("Wrong entry, please repeat: ");
                }
            } while (!choice.matches("[0-9]{1,2}"));
            switch (choice) {
                case "0":
                    con.deleteData();
                    System.out.println("Good bye :)");
                    break;
                case "1": // Add a class
                    Jahrgang c = new Jahrgang();
                    do {
                        System.out.print("Enter class' ID: ");
                        c.setStammklasse(scan.nextLine());
                    } while (c.getStammklasse().matches("[0-9]*"));
                    do {
                        System.out.print("Enter year of class: ");
                        c.setYear(scan.nextLine());
                    } while (!c.getYear().matches("[0-9]*"));
                    jahrgang.save(c);
                    jahrgang.showAllClasses();

                    break;
                case "2": // add teacher
                    Lehrer t = new Lehrer();

                    String lname, lyear, lloc;
                    do {
                        System.out.print("Enter name of teacher: ");
                        lname = scan.nextLine();
                    } while (lname.matches("[0-9]*"));
                    do {
                        System.out.print("Enter date of birth: ");
                        lyear = scan.nextLine();
                    } while (!lyear.matches("[0-9]*"));
                    do {
                        System.out.print("Enter location of teacher: ");
                        lloc = scan.nextLine();
                    } while (lloc.matches("[0-9]*"));

                    Lehrer l = new Lehrer(lname, lyear, lloc);
                    teacher.save(l);
                    teacher.showAllTeacher();

                    break;
                case "3": // add pupil
                    String sname, sGeb, sMatnr;
                    do {
                        System.out.print("Enter name of pupil: ");
                        sname = scan.nextLine();
                    } while (sname.matches("[0-9]*"));
                    do {
                        System.out.print("Enter date of birth: ");
                        sGeb = scan.nextLine();
                    } while (!sGeb.matches("[0-9]*"));
                    do {
                        System.out.print("Enter matrikel-number: ");
                        sMatnr = scan.nextLine();
                    } while (!sMatnr.matches("[0-9]*"));
                    Student s = new Student(sname, sGeb, sMatnr);
                    student.save(s);
                    student.showAllPupil();

                    break;
                case "4": // add subject
                    String fname, fhours;
                    do {
                        System.out.print("Enter subject: ");
                        fname = scan.nextLine();
                    } while (fname.matches("[0-9]*"));
                    do {
                        System.out.print("Enter weekly hours of subject: ");
                        fhours = scan.nextLine();
                    } while (!fhours.matches("[0-9]*"));

                    Faecher f = new Faecher(fname, fhours);
                    subject.save(f);
                    subject.showAllSubjects();
                    break;
                case "5": // Add pupil to class
                    do {
                        System.out.print("Enter class' ID or 'h' for help: ");
                        number = scan.nextLine();
                        if (number.equals("h")) {
                            jahrgang.showAllClasses();
                        }
                    } while (!number.matches("[0-9]*"));
                    do {
                        System.out.print("Enter pupil's ID or 'h' for help: ");
                        name = scan.nextLine();
                        if (name.equals("h")) {
                            student.showAllPupil();
                        }
                    } while (!name.matches("[0-9]*"));
                    student.addToClass(number, name);
                    System.out.println("Pupil added successfully to class.");
                    break;
                case "6": // Add pupil to teacher
                    do {
                        System.out.print("Enter teacher's ID or 'h' for help: ");
                        number = scan.nextLine();
                        if (number.equals("h")) {
                            teacher.showAllTeacher();
                        }
                    } while (!number.matches("[0-9]*"));
                    do {
                        System.out.print("Enter pupil's ID or 'h' for help: ");
                        name = scan.nextLine();
                        if (name.equals("h")) {
                            student.showAllPupil();
                        }
                    } while (!name.matches("[0-9]*"));
                    student.addPupilToTeacher(number, name);
                    break;
                case "7": // add subject to teacher
                    do {
                        System.out.print("Enter teacher's ID  or 'h' for help: ");
                        number = scan.nextLine();
                        if (number.equals("h")) {
                            teacher.showAllTeacher();
                        }
                    } while (!number.matches("[0-9]*"));
                    do {
                        System.out.print("Enter subject's ID or 'h' for help: ");
                        name = scan.nextLine();
                        if (name.equals("h")) {
                            subject.showAllSubjects();
                        }
                    } while (!name.matches("[0-9]*"));
                    teacher.addSubjectToTeacher(number, name);
                    break;
                case "8": // add grades to pupil
                    String sID, lID, fID, bNote, bDatum;
                    do {
                        System.out.print("Enter pupil's ID or 'h' for help: ");
                        sID = scan.nextLine();
                        if (sID.equals("h")) {
                            sql = "SELECT * FROM tbl_schueler";
                            con.SELECT(sql);
                        }
                    } while (!sID.matches("[0-9]*"));
                    do {
                        System.out.print("Enter teachers's ID or 'h' for help: ");
                        lID = scan.nextLine();
                        if (lID.equals("h")) {
                            sql = "SELECT * FROM tbl_lehrer";
                            con.SELECT(sql);
                        }
                    } while (!lID.matches("[0-9]*"));
                    do {
                        System.out.print("Enter subject's ID: ");
                        fID = scan.nextLine();
                        if (fID.equals("h")) {
                            sql = "SELECT * FROM tbl_faecher";
                            con.SELECT(sql);
                        }
                    } while (!fID.matches("[0-9]*"));
                    do {
                        System.out.print("Enter grade: ");
                        bNote = scan.nextLine();
                    } while (!bNote.matches("[0-5]"));
                    do {
                        System.out.print("Enter date: ");
                        bDatum = scan.nextLine();
                    } while (bDatum.matches("[a-zA-Z]*"));
                    sql = "INSERT IGNORE INTO tbl_beurteilung (sID, lID, fID, bNote, bDatum) VALUES (" + sID + ", " + lID + ", " + fID + ", " + bNote + ", STR_TO_DATE('" + bDatum + "',GET_FORMAT(DATE,'EUR')));";
                    con.SELECT(sql);
                    break;
                case "9": // show all pupil of a class
                    do {
                        System.out.print("Enter class ID or 'h' for help: ");
                        number = scan.nextLine();
                        if (number.equals("h")) {
                            sql = "SELECT * FROM tbl_jahrgang";
                            con.SELECT(sql);
                        }
                    } while (!number.matches("[0-9]*"));
                    student.showAllPupilOfAClass(number);
                    break;
                case "10": // pupil of a teacher
                    do {
                        System.out.print("Enter teacher's ID or 'h' for help: ");
                        number = scan.nextLine();
                        if (number.equals("h")) {
                            sql = "SELECT * FROM tbl_lehrer";
                            con.SELECT(sql);
                        }
                    } while (!number.matches("[0-9]*"));
                    student.showAllPupilOfTeacher(number);
                    break;
                case "11": // all subjects of a teacher
                    String teacherID = "1";
                    teacher.allSubjectsOfATeacher(teacherID);
                    break;
                case "12": // all subjects of a  pupil
                    String pupilID = "1";
                    break;
                case "13": // all teacher of a subject
                    break;
                case "14": // all pupil of a subject
                    break;
                case "15": // grades of a pupil
                    do {
                        System.out.print("Enter pupil's ID or 'h' for help: ");
                        number = scan.nextLine();
                        if (number.equals("h")) {
                            student.showAllPupil();
                        }
                    } while (!number.matches("[0-9]*"));
                    sql = "SELECT * FROM tbl_beurteilung WHERE sID = " + number + ";";
                    con.SELECT(sql);
                    break;
                case "99": // indiviudeller SELECT SQL-Befehl
                    System.out.print("Enter SELECT > ");
                    sql = scan.nextLine();
                    con.SELECT(sql);
                    break;
                default:  // should not be accessible
                    System.out.println("Not good if you see this ...");
                    break;
            }
        } while (!choice.matches("0"));
    }
}