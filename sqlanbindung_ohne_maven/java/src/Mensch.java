import java.util.*;
public class Mensch {
    public static String number, sql, name, year;
    public MySQLConnect con = new MySQLConnect();

    public Mensch() {

    }
    public  Mensch(String n, String y) {
       setName(n);
       setYear(y);
    }

    public static void setName(String name) {
        if(name.length() < 11) {
            Mensch.name = name;
        } else {
            System.out.println("Please enter a name with max-length: 10");
            setName(new Scanner(System.in).nextLine());
        }
    }

    public static void setYear(String year) {
        if(!year.matches("[a-zA-Z]*")) {
            Mensch.year = year;
        } else {
            System.out.print("Please enter a valid date: ");
            setYear(new Scanner(System.in).nextLine());
        }
    }

    public String getName() {
        return name;
    }

    public String getYear() {
        return year;
    }
}
