/*
https://stackoverflow.com/questions/2839321/connect-java-to-a-mysql-database
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

public class MySQLConnect {
    private static final String DATABASE_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/regtool?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    private static final String MAX_POOL = "250";
    private static String sql;
    private static Scanner scan = new Scanner(System.in);
    private Connection connection;
    private Properties properties;

    public void INSERT(String sql) {
        try {
            Statement statement = connect().createStatement();
            statement.executeUpdate(sql);
        }
        catch (SQLIntegrityConstraintViolationException g) {
            System.out.println("Data already saved in database. No duplicates allowed.");
            System.out.println("Do you want to override?");
            pressAnyKeyToContinue();
        }catch (SQLException e) {
            System.out.println("### An error occurred ###");
            System.out.println(e);
            pressAnyKeyToContinue();
        }
        finally {
            disconnect();
        }
    }

    public void SELECT(String sql) {
        try {
            PreparedStatement statement = connect().prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            ResultSetMetaData meta = rs.getMetaData();
            int col = meta.getColumnCount();
            String header = "";
            for (int i = 1; i <= col; i++) {
                //System.out.print(meta.getColumnName(i) + "\t");
                System.out.print(meta.getColumnName(i) + "\t\t");
            }
            System.out.println(header);
            while (rs.next()) {
                for (int i = 1; i <= col; i++) {
                    System.out.print(rs.getString(i) + "\t\t");
                }
                System.out.println();
            }
        } catch (SQLSyntaxErrorException f) {
            System.out.println("Selected table doesn't exist");
        } catch (SQLException e) {
            System.out.println("### An error occurred! ###");
            System.out.println(e);
            pressAnyKeyToContinue();
        } finally {
            disconnect();
        }
    }

    public int NumberOfRows(String selectCountTableName) {
        try {
            Statement stmt = connect().createStatement();
            ResultSet rs = stmt.executeQuery("SELECT count(*) FROM " + selectCountTableName);
            rs.next();
            return rs.getInt("count(*)");
        } catch (SQLException e) {
            System.out.println("### An error occurred! ###");
            pressAnyKeyToContinue();
        }
        return 0;
    }

    public void DELETE(String sql) {
        try {
            Statement statement = connect().createStatement();
            statement.execute(sql);
        } catch (SQLException e) {
            System.out.println("### An error occurred! ###");
            pressAnyKeyToContinue();
        } finally {
            disconnect();
        }
    }

    public void UPDATE(String sql) {
        try {
            Statement statement = connect().createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            System.out.println("### An error occurred! ###");
            pressAnyKeyToContinue();
        } finally {
            disconnect();
        }
    }

    private Properties getProperties() {
        if (properties == null) {
            properties = new Properties();properties.setProperty("user", USERNAME);
            properties.setProperty("password", PASSWORD);
            properties.setProperty("MaxPooledStatements", MAX_POOL);
        }
        return properties;
    }

    public Connection connect() {
        if (connection == null) {
            try {
                Class.forName(DATABASE_DRIVER);
                connection = DriverManager.getConnection(DATABASE_URL, getProperties());
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

    public void disconnect() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                System.out.println("### An error occurred! ###");
                pressAnyKeyToContinue();
            }
        }
    }

    public void deleteData() {
        try {
            Statement statement = connect().createStatement();
            statement.executeUpdate("SET FOREIGN_KEY_CHECKS = 0;");
            statement.executeUpdate("TRUNCATE tbl_beurteilung;");
            statement.executeUpdate("TRUNCATE tbl_schueler;");
            statement.executeUpdate("TRUNCATE tbl_lehrer;");
            statement.executeUpdate("TRUNCATE tbl_jahrgang;");
            statement.executeUpdate("TRUNCATE tbl_faecher;");
            statement.executeUpdate("SET FOREIGN_KEY_CHECKS = 1;");
        } catch (SQLException e) {
            System.out.println("### An error occurred! ###");
            pressAnyKeyToContinue();
        } finally {
            disconnect();
        }
    }

    private void pressAnyKeyToContinue() {
        System.out.println("Press Enter key to continue ...");
        System.out.print("> ");
        try {
            System.in.read();
        } catch (Exception e) {
        }
    }
}