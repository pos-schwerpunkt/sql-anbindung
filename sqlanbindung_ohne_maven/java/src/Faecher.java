import java.util.Scanner;

public class Faecher {
    private String bezeichnung, hours, sql;
    public MySQLConnect con = new MySQLConnect();

    public Faecher() {

    }
    public Faecher(String name, String hour) {
        setBezeichnung(name);
        setHours(hour);
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public String getHours() {
        return hours;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public void setHours(String h) {
        if(h.matches("[0-9]{1,2}")) {
            this.hours = h;
        } else {
            System.out.println("Wrong entry, please repeat: ");
            setHours(new Scanner(System.in).nextLine());
        }
    }
    public void save(Faecher subject) {
        sql = "INSERT INTO tbl_faecher (fName, fHours) VALUES ('" + subject.getBezeichnung() + "'," + subject.getHours() + ");";
        con.INSERT(sql);
    }

    public void showAllSubjects() {
        sql = "SELECT * FROM tbl_faecher";
        con.SELECT(sql);
    }

}
