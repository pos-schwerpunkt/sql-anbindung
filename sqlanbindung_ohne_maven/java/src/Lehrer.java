public class Lehrer extends Mensch {
    private String location;
    public Lehrer() {}

    public Lehrer(String n, String g, String l) {
        super(n, g);
        setLocation(l);
    }

    public String getLocation() {
        return location;
    }



    public void setLocation(String loc) {
        this.location = loc;
    }

    public void save(Lehrer lehrer) {
        sql = "INSERT IGNORE INTO tbl_lehrer (lName, lGeb, lLocation) VALUES ('" + lehrer.getName() + "', STR_TO_DATE('" + lehrer.getYear() + "', GET_FORMAT(DATE,'EUR')), '" + lehrer.getLocation() + "');";
        con.INSERT(sql);
    }

    public void showAllTeacher() {
        sql = "SELECT * FROM tbl_lehrer";
        con.SELECT(sql);
    }

    public void showAllPupilOfTeacher(String IndexOfTeacher) {
        sql = "SELECT sl.slID AS ID, sName AS Schülername, s.sID as ID, s.sMatrikelnr AS Nummer, t.lName AS Lehrperson, t.lID AS ID FROM tbl_schueler_lehrer sl " +
                "INNER JOIN tbl_schueler s ON sl.sID = s.sID " +
                "INNER JOIN tbl_lehrer t ON sl.lID = t.lID " +
                "WHERE t.lID = " + IndexOfTeacher + ";";
        con.SELECT(sql);
    }

    public void addTeacherToPupil(String TeacherID, String StudentID) {
        sql = "INSERT IGNORE INTO tbl_schueler_lehrer (lID, sID) VALUES (" + TeacherID + ", " + StudentID + ");";
        con.INSERT(sql);
    }

    public void addSubjectToTeacher(String subject, String teacher) {
        sql = "INSERT IGNORE INTO tbl_faecher_specs (fID, lID) VALUES (" + subject + ", " + teacher + ");";
        con.INSERT(sql);
    }

    public void allSubjectsOfATeacher(String teacherID) {
        sql = "SELECT fID AS ID, lID AS Teacher FROM tbl_faecher_specs WHERE lID = " + teacherID + ";";
        con.SELECT(sql);
    }
}
