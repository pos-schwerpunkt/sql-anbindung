import java.util.Scanner;

public class Jahrgang {
    private String stammklasse, year, sql;
    public MySQLConnect con = new MySQLConnect();

    public Jahrgang() {

    }
    public Jahrgang(String name, String year) {
        setStammklasse(name);
        setYear(year);
    }

    public String getStammklasse() {
        return stammklasse;
    }

    public String getYear() {
        return year;
    }

    public void setStammklasse(String stammklasse) {
        this.stammklasse = stammklasse;
    }

    public void setYear(String y) {
        if(y.matches("[0-9]{4}")) {
            this.year = y;
        } else {
            System.out.println("Wrong entry, please repeat: ");
            setYear(new Scanner(System.in).nextLine());
        }
    }
    public void save(Jahrgang klasse) {
        sql = "INSERT INTO tbl_jahrgang (jName, jJahr) VALUES ('" + klasse.getStammklasse() + "'," + klasse.getYear() + ");";
        con.INSERT(sql);
    }

    public void showAllClasses() {
        sql = "SELECT * FROM tbl_jahrgang";
        con.SELECT(sql);
    }

}
