import java.util.Scanner;

public class Student extends Mensch {
    private static String mnr;

    public Student() {}
    public Student (String n, String y, String nr) {
        super(n, y);
        setMatrikelnummer(nr);
        System.out.println("Pupil saved to database.");
    }

    public void setMatrikelnummer(String matrikelnummer) {
        if(matrikelnummer.matches("[0-9]+")) {
            Student.mnr = matrikelnummer;
        } else {
            System.out.print("Wrong number, please repeat: ");
            setMatrikelnummer(new Scanner(System.in).nextLine());
        }
    }

    public static String getMnr() {
        return mnr;
    }

    public void save(Student pupil) {
        sql = "INSERT IGNORE INTO tbl_schueler (sName, sGeb, sMatrikelNr) VALUES ('" + pupil.getName() + "', STR_TO_DATE('" + pupil.getYear()+ "', GET_FORMAT(DATE,'EUR')), " + getMnr() + ");";
        con.INSERT(sql);
    }

    public void addToClass(String StudentID, String ClassID) {
        sql = "UPDATE tbl_schueler set jID = '" + ClassID + "' WHERE sID = '" + StudentID + "';";
        con.UPDATE(sql);
    }

    public void addHeadTeacherToPupil(String StudentID, String TeacherID) {
        sql = "UPDATE tbl_schueler set lID = '" + TeacherID + "' WHERE sID = '"+ StudentID + "';";
        con.UPDATE(sql);
    }

    public void addPupilToTeacher(String TeacherID, String StudentID) {
        sql = "INSERT INTO tbl_schueler_lehrer (lID, sID) VALUES (" + TeacherID + ", " + StudentID + ");";
        con.INSERT(sql);
    }

    public void showAllPupilOfTeacher(String StudentID) {
        sql = "SELECT sl.slID AS ID, sName AS Name, s.sID as ID, s.sMatrikelnr AS Nummer, t.lName AS Teacher, t.lID AS ID FROM tbl_schueler_lehrer sl \n" +
                "INNER JOIN tbl_schueler s ON sl.sID = s.sID " +
                "INNER JOIN tbl_lehrer t ON sl.lID = t.lID " +
                "WHERE s.lID = " + StudentID + ";";
        con.SELECT(sql);
    }

    public void showAllPupilOfAClass(String StudentID) {
        sql = "SELECT * FROM tbl_schueler WHERE jID = " + StudentID + ";";
        con.SELECT(sql);
    }

    public void showAllPupil() {
        sql = "SELECT * FROM tbl_schueler;";
        con.SELECT(sql);
    }

    public void showAllSubjectsOfAPupil(String pupilID) {
        sql = "SELECT fID AS ID, sID AS Pupil FROM tbl_faecher_specs WHERE sID = " + pupilID + ";";
        con.SELECT(sql);
    }
}
